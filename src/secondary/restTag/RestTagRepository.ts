import {ITagRepository} from "../../domain/tag/ITagRepository";
import {injectable} from "inversify";
import {baseAxios} from "../../axiosConfig";
import {AxiosResponse} from "axios";
import {RestTag, toTagList} from "./RestTag";
import {Tag} from "../../domain/tag/Tag";

@injectable()
export class RestTagRepository implements ITagRepository {
    getTagList(): Promise<Tag[]> {
        return baseAxios.get('/tags').then((response: AxiosResponse<RestTag[]>) => {
            return response.data.map(toTagList);
        }).catch((error) => {
            throw Error(error.message);
        })
    }

}