import {Tag} from "../../domain/tag/Tag";

export interface RestTag {
    _id: string;
    name: string;
}

export const toTagList = (restTag: RestTag): Tag => ({
    id: restTag._id,
    name: restTag.name,
});