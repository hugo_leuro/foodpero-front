import {injectable} from "inversify";
import {IDishRepository} from "../../domain/dish/IDishRepository";
import {baseAxios} from "../../axiosConfig";
import {AxiosResponse} from "axios";
import {RestDish, toDish} from "./RestDish";
import {IDish} from "../../domain/dish/Dish";

@injectable()
export class RestDishRepository implements IDishRepository {

    addDish(dish: FormData): Promise<void> {
        return baseAxios.post('/dishes', dish).then(() => {
            console.log("Produit ajouté");
        }).catch((error) => {
            throw Error(error.message);
        })
    }

    getDishes(): Promise<IDish[]> {
        return baseAxios.get('/dishes').then((response: AxiosResponse<RestDish[]>) => {
            return response.data.map(toDish);
        }).catch((error) => {
            throw Error(error.message);
        })
    }
}