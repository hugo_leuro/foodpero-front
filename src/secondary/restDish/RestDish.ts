import {IDish} from "../../domain/dish/Dish";
import {RestTag} from "../restTag/RestTag";

export interface RestDish {
    name: string;
    tags: RestTag[];
    dishImages: string[];
    time: number;
}

export const toDish = (restDish : RestDish): IDish => ({
    name: restDish.name,
    images: restDish.dishImages.map(toImage),
    tags: restDish.tags.map(toTag),
    time: restDish.time,
})

const toImage = (restDishImage: string) => {
    return process.env.REACT_APP_BASE_URL + restDishImage;
}

const toTag = (restTagDish: RestTag): string => {
    return restTagDish.name;
}