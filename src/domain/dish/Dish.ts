export interface IDish {
    name: string;
    tags: string[];
    images: string[];
    time: number;
}