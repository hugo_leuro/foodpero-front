import {IDish} from "./Dish";

export interface IDishRepository {
    addDish(dish: FormData): Promise<void>;
    getDishes(): Promise<IDish[]>
}