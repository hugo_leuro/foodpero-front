import {Tag} from "./Tag";

export interface ITagRepository {
    getTagList(): Promise<Tag[]>;
}