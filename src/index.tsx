import "reflect-metadata";

import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import {BrowserRouter} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import App from "./primary/components/app/App";
import {Provider} from "inversify-react";


import {Container} from 'inversify';
import {ITagRepository} from "./domain/tag/ITagRepository";
import {RestTagRepository} from "./secondary/restTag/RestTagRepository";
import {RestDishRepository} from "./secondary/restDish/RestDishRepository";
import {IDishRepository} from "./domain/dish/IDishRepository";

const container = new Container();
container.bind<ITagRepository>('restTagRepository').to(RestTagRepository);
container.bind<IDishRepository>('restDishRepository').to(RestDishRepository);

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Provider container={container}>
                <App/>
            </Provider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
