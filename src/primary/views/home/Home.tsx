import React from 'react';
import {TagList} from "../../components/tagList/TagList";
import {DishList} from "../../components/dishList/DishList";
import {ITagRepository} from "../../../domain/tag/ITagRepository";
import {resolve} from "inversify-react";
import {Tag} from "../../../domain/tag/Tag";
import './Home.scss';
import {EventBus} from "../../utils/EventBus";
import search from './search.png';
import filter from './filter.png';
import {FilterPanel} from "../../components/filterPanel/FilterPanel";
import {AddButton} from "../../components/addButton/AddButton";
import {IDishRepository} from "../../../domain/dish/IDishRepository";
import {IDish} from "../../../domain/dish/Dish";

type IState = {
    tags: Tag[];
    selectedTags: string[];
    dishes: IDish[];
    updatedDishes: IDish[];
    filterSearch: string;
}

class Home extends React.Component<any, IState> {
    @resolve("restTagRepository") private readonly tagRepository!: ITagRepository;
    @resolve("restDishRepository") private readonly dishRepository!: IDishRepository;

    constructor(props: any) {
        super(props);
        this.state = {
            tags: [],
            selectedTags: [],
            dishes: [],
            updatedDishes: [],
            filterSearch: '',
        }
        this.showFilter = this.showFilter.bind(this);
        this.setSearchValue = this.setSearchValue.bind(this);
    }

    componentDidMount() {
        EventBus.on('selectedTags', (tags: string[]) => {
            this.setState({selectedTags: tags}, this.filterByTag);
        });
        EventBus.on('updateDishList', () => this.getDishes());
        this.tagRepository.getTagList().then(tags => this.setState({tags: tags}));
        this.getDishes();
    }

    componentWillUnmount() {
        EventBus.remove('selectedTags');
    }

    private filterByTag() {
        const dishes = [...this.state.dishes];
        if (this.state.selectedTags.length > 0) {
            const dishUpdate = dishes.filter(dish => this.state.selectedTags.some(selectedTag => dish.tags.includes(selectedTag)));
            this.setState({updatedDishes: dishUpdate});
        } else this.setState({updatedDishes: dishes})
    }

    private getDishes() {
        this.dishRepository.getDishes().then(dishes => this.setState({dishes: dishes, updatedDishes: dishes}))
    }

    private showFilter() {
        EventBus.dispatch('showFilterPanel', true);
    }

    private setSearchValue(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({filterSearch: event.target.value});
    }

    render() {
        const lowercasedFilter = this.state.filterSearch.toLowerCase();
        let filteredSearchData = this.state.updatedDishes.filter((dish: IDish) => dish.name.toLowerCase().includes(lowercasedFilter));

        return (
            <div className="home">
                <h1 className="home__title">Food
                    <span className="home__title--accent">'</span>
                    Péro</h1>
                <div className="home__search">
                    <img src={search} className="home__iconSearch" alt=""/>
                    <input type="text" placeholder="Rechercher un plat" value={this.state.filterSearch}
                           className="home__input" onChange={this.setSearchValue}/>
                    <img src={filter} className="home__iconFilter" onClick={this.showFilter} alt=""/>
                </div>
                <AddButton tags={this.state.tags}/>
                <TagList tags={this.state.selectedTags} key={'taglist-' + this.state.selectedTags.length}/>
                <DishList dishes={filteredSearchData} key={'dishlist-' + filteredSearchData}/>
                <FilterPanel tags={this.state.tags} selectedTags={this.state.selectedTags}/>


            </div>
        );
    }
}

export default Home;
