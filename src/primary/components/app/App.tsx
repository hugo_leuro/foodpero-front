import React from 'react';
import './App.scss';
import {Route, Routes} from "react-router-dom";
import Home from "../../views/home/Home";
import {ModalDish} from "../modalDish/ModalDish";
import {EventBus} from "../../utils/EventBus";
import {Tag} from "../../../domain/tag/Tag";

type IState = {
    tags: Tag[];
}

export class App extends React.Component<any, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            tags: [],
        }
    }

    componentDidMount() {
        EventBus.on('showModal', (data: Tag[]) => this.setState({tags: data}));
    }

    render() {
        return (
            <div className={'app ' + (this.state.tags.length > 0 ? 'overflow' : '')}>
                {this.state.tags.length > 0 && <div className="overlay"></div>}
                {this.state.tags.length > 0 && <ModalDish tags={this.state.tags}/>}
                <Routes>
                    <Route path='/' element={<Home/>}/>
                </Routes>
            </div>
        );
    }
}

export default App;
