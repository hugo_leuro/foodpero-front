import React from "react";
import {IDish} from "../../../domain/dish/Dish";
import {Dish} from "../dish/Dish";
import './DishList.scss';

type IProps = {
    dishes: IDish[];
}

export class DishList extends React.Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        return (
            <div className="dishlist">
                {this.props.dishes.map((dish, index) => (

                    <Dish dish={dish} key={index}/>
                ))}
            </div>
        )
    }
}