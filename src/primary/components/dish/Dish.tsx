import React from "react";
import {IDish} from "../../../domain/dish/Dish";
import './Dish.scss'
import {ReactComponent as Clock} from './clock.svg';

type IProps = {
    dish: IDish;
}

export class Dish extends React.Component<IProps, any> {
    render() {
        return (
            <div className="dish">
                <div className="dish__img">
                    <img src={this.props.dish.images[0]}
                         onError={(e) => (e.target as HTMLImageElement).src = "img/default.jpeg"}/>
                    <div className="dish__time">
                        <Clock/>
                        {this.props.dish.time} min
                    </div>
                </div>
                <div className="dish__content">
                    <h2 className="dish__title">{this.props.dish.name}</h2>
                    <div className="dish__tags">
                        {this.props.dish.tags.map((tag, index) => (
                            <div className="dish__tag" key={index}>{tag}</div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}