import React from "react";
import './AddButton.scss';
import {EventBus} from "../../utils/EventBus";
import {Tag} from "../../../domain/tag/Tag";

type IProp = {
    tags: Tag[];
}

export class AddButton extends React.Component<IProp, any> {
    constructor(props: IProp) {
        super(props);

        this.showModal = this.showModal.bind(this);
    }

    componentDidMount() {
        const button = document.querySelector('.add')!;
        const span = document.querySelector('.add--background')! as HTMLSpanElement;
        this.setSpanPositionMouseEnter(button, span);
        this.setSpanPositionMouseOut(button, span);
    }

    private setSpanPositionMouseOut(button: Element, span: HTMLSpanElement) {
        button.addEventListener('mouseout', (event: Event) => {
            const offset = button.getBoundingClientRect();
            let relX = (event as MouseEvent).clientX - offset.left
            let relY = (event as MouseEvent).clientY - offset.top

            span.style.top = String(relY) + "px";
            span.style.left = String(relX) + "px";
        })
    }

    private setSpanPositionMouseEnter(button: Element, span: HTMLSpanElement) {
        button.addEventListener('mouseenter', (event: Event) => {
            const offset = button.getBoundingClientRect();

            let relX = (event as MouseEvent).clientX - offset.left
            let relY = (event as MouseEvent).clientY - offset.top

            span.style.top = String(relY) + "px";
            span.style.left = String(relX) + "px";
        })
    }

    private showModal() {
        EventBus.dispatch('showModal', this.props.tags);
    }

    render() {
        return (
            <div className="add" onClick={this.showModal}>
                <span className="add--background"></span>
                +
            </div>
        )
    }
}