import React from "react";
import './TagList.scss';
import {EventBus} from "../../utils/EventBus";

type IProps = {
    tags: string[]
};

type IState = {
    selectedTags: string[];
}

export class TagList extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            selectedTags: props.tags,
        }
    }

    private removeTag(tag: string) {
        const tags: string[] = [...this.state.selectedTags];
        tags.splice(tags.indexOf(tag), 1);

        this.setState({selectedTags: tags});
        EventBus.dispatch('selectedTags', tags);
    }

    render() {
        return (
            <div className="taglist">
                {this.state.selectedTags.map((tag, index) => (
                    <div className="taglist__tag" key={index}>
                        <span className="taglist__tag__close" onClick={() => this.removeTag(tag)}></span>
                        {tag}
                    </div>
                ))}
            </div>
        );
    }
}