import React from "react";
import './ModalDish.scss';
import {Tag} from "../../../domain/tag/Tag";
import {resolve} from "inversify-react";
import {IDishRepository} from "../../../domain/dish/IDishRepository";
import {EventBus} from "../../utils/EventBus";
import arrow from './arrow-down.png';

type IProps = {
    tags: Tag[];
}

export type IStateDish = {
    dishName: string;
    tags: Tag[];
    images: File[];
    time: string;
    showTag: boolean;
}

export class ModalDish extends React.Component<IProps, IStateDish> {
    @resolve("restDishRepository") private readonly dishRepository!: IDishRepository;

    constructor(props: IProps) {
        super(props);
        this.state = {
            dishName: '',
            tags: [],
            images: [],
            time: '',
            showTag: false,
        }

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.selectTag = this.selectTag.bind(this);
        this.handleChangeFiles = this.handleChangeFiles.bind(this);
        this.addDish = this.addDish.bind(this);
        this.showTags = this.showTags.bind(this);
        this.close = this.close.bind(this);
    }

    componentDidMount() {
        this.removeImage();
    }

    private handleChangeName(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({dishName: event.target.value})
    }

    private handleChangeTime(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({time: event.target.value})
    }

    private removeImage() {
        const removeButton = document.querySelector('.remove-img');
        const imgWrapper = document.querySelector('.file-input');
        if (removeButton) {
            removeButton!.addEventListener('click', (e) => {
                const fileDiv = removeButton!.parentElement;
                const files = [...this.state.images];
                const filesUpdated = [...this.state.images];

                files.splice(files.indexOf(filesUpdated.filter(file => file.name === fileDiv!.dataset.name!)[0]), 1)

                this.setState({images: files})
                imgWrapper!.removeChild(fileDiv!);
            })
        }
    }

    private handleChangeFiles(event: React.ChangeEvent<HTMLInputElement>) {
        let files: File[] = []
        const imgWrapper = document.querySelector('.file-input');
        for (let i = 0; i < event.target.files!.length; i++) {
            files.push(event.target.files!.item(i)!);
            const reader = new FileReader();
            reader.addEventListener('load', (e) => {
                const itemWrapper = this.createFileImageHtml(e, event, i);

                imgWrapper!.append(itemWrapper);
            })
            reader.readAsDataURL(event.target.files!.item(i)!);
        }

        this.setState({images: files})
    }

    private createFileImageHtml(e: ProgressEvent<FileReader>, event: React.ChangeEvent<HTMLInputElement>, i: number) {
        const itemWrapper = document.createElement('div');
        const img = document.createElement('img');
        const removeImg = document.createElement('div');

        itemWrapper.className = 'file-input__img';
        removeImg.className = 'remove-img';

        img.src = e.target!.result! as string;
        itemWrapper.dataset.name = event.target.files!.item(i)!.name;

        itemWrapper.append(img);
        itemWrapper.append(removeImg);
        return itemWrapper;
    }

    private selectTag(tag: Tag) {
        let tags: Tag[] = [...this.state.tags];
        if (tags.indexOf(tag) !== -1) tags.splice(tags.indexOf(tag), 1);
        else tags.push(tag);

        this.setState({tags: tags});
    }

    private removeTag(tag: Tag) {
        const tags: Tag[] = [...this.state.tags];
        tags.splice(tags.indexOf(tag), 1);

        this.setState({tags: tags});
    }

    private addDish() {
        const formData = this.createFormData();
        this.close();
        this.dishRepository.addDish(formData).then(() => {

            EventBus.dispatch('updateDishList', true);
        }).catch((error) => {
            console.log(error);
        })
    }

    private close() {
        EventBus.dispatch('showModal', []);
    }

    private createFormData() {
        const formData = new FormData();
        formData.append('name', this.state.dishName);
        formData.append('time', this.state.time);
        this.state.tags.forEach(tag => {
            formData.append('tags', tag.id);
        })
        this.state.images.forEach(image => {
            formData.append('dishImages', image);
        })
        return formData;
    }

    private showTags() {
        this.setState({showTag: !this.state.showTag});
    }

    render() {
        return (
            <div className="modaldish">
                <span className="modaldish__close" onClick={this.close}></span>
                <h2 className="modaldish__title">Envie de rajouter un plat ?</h2>
                <div className="modaldish__content">
                    <div className="modaldish__inputgroup col-1">
                        <label className="modaldish__labelname" htmlFor="">Nom du plat</label>
                        <input type="text" placeholder="Pizza, lasagne..." name="dishName" value={this.state.dishName}
                               onChange={this.handleChangeName}/>
                    </div>
                    <div className="modaldish__inputgroup col-1">
                        <label className="modaldish__labelname" htmlFor="">Durée de prépration</label>
                        <input type="text" placeholder="90, 60..." name="time" value={this.state.time}
                               onChange={this.handleChangeTime}/>


                    </div>
                    <div className="modaldish__inputgroup col-2">
                        <label className="modaldish__labelname" htmlFor="">Type de plat</label>
                        <div className={"modaldish__selectedtags " + (this.state.showTag ? '--open' : '')}>
                            {this.state.tags.map((tag, i) => (
                                <div className="modaldish__selectedtag" onClick={() => this.selectTag(tag)} key={i}>
                                    {tag.name}
                                    <span className="modaldish__selectedtag__close"
                                          onClick={() => this.removeTag(tag)}></span>
                                </div>
                            ))}
                            <img src={arrow}
                                 className={"modaldish__selectedtags__arrow " + (this.state.showTag ? '--opened' : '')}
                                 onClick={this.showTags}></img>
                        </div>
                        <div className={"modaldish__tags " + (this.state.showTag ? '--show' : '')}>
                            {this.props.tags.map((item, i) => (
                                <div className="modaldish__tag" onClick={() => this.selectTag(item)}
                                     key={i}>{item.name}</div>
                            ))}
                        </div>
                    </div>

                    <div className="modaldish__inputgroup col-2">
                        <label className="modaldish__labelname" htmlFor="">Images</label>
                        <div className="file-input">
                            <div className="file-input-item">
                                <label htmlFor="file" className="file-input-item--content">+</label>
                            </div>
                            <input className="input-file" type="file" id="file" multiple name="images"
                                   onChange={this.handleChangeFiles}/>
                        </div>

                    </div>

                    <div className="modaldish__add" onClick={this.addDish}>
                        <span></span>
                        Ajouter
                    </div>
                </div>
            </div>
        )
    }
}