import React from "react";
import {Tag} from "../../../domain/tag/Tag";
import './FilterPanel.scss'
import {EventBus} from "../../utils/EventBus";

type IProp = {
    tags: Tag[];
    selectedTags: string[];
}

type IState = {
    selectedTags: string[];
    show: boolean;
}

export class FilterPanel extends React.Component<IProp, IState> {
    constructor(props: IProp) {
        super(props);

        this.state = {
            selectedTags: props.selectedTags,
            show: false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.close = this.close.bind(this);
        this.clear = this.clear.bind(this);
    }

    componentDidMount() {
        EventBus.on('showFilterPanel', (data: boolean) => this.setState({show: data}));
    }

    componentWillUnmount() {
        EventBus.remove('showFilterPanel');
    }

    private close() {
        this.setState({show: false});
    }

    private clear() {
        this.setState({selectedTags: []});
        EventBus.dispatch('selectedTags', []);
    }

    private handleChange(tag: string) {
        const tags: string[] = [...this.state.selectedTags];

        if (tags.includes(tag)) tags.splice(tags.indexOf(tag), 1);
        else tags.push(tag);

        this.setState({selectedTags: tags});
        EventBus.dispatch('selectedTags', tags);
    }

    render() {
        return (
            <div className={"filterpanel " + (this.state.show ? '--opened' : '')}>
                <div className="filterpanel__header">
                    <p className={"filterpanel__header__item filterpanel__header__clear " + (this.state.selectedTags.length === 0 ? '--empty' : '')}
                       onClick={this.clear}>Nettoyer</p>
                    <p className="filterpanel__header__item filterpanel__header__close" onClick={this.close}>Fermer</p>
                </div>
                <div className="filterpanel__content">
                    <div className="filterpanel__item">
                        <p className="filterpanel__title">Tag</p>
                        {this.props.tags.map((tag, index) => (
                            <label className="filterpanel__tag" htmlFor={tag.name + "-" + index}
                                   key={index + this.state.selectedTags.length}>
                                {tag.name}
                                <input type="checkbox" onChange={() => this.handleChange(tag.name)} value={tag.name}
                                       checked={this.state.selectedTags.some(selectedTag => selectedTag === tag.name)}
                                       id={tag.name + "-" + index}/>
                            </label>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}